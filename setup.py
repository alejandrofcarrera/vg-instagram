try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='instagram_api',
    version='1.0.0',
    author='alejfcarrera',
    author_email='alejfcarrera@mail.ru',
    license='MIT',
    url='https://gitlab.com/alejandrofcarrera/vg-instagram.git',
    install_requires=[],
    keywords='instagram api library client wrapper',
    description='A client interface for Instagram API.',
    packages=['instagram_api'],
    classifiers=[]
)
