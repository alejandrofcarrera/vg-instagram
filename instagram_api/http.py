#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This file has all the necessary functions to generate
the HTTP methods to make requests correctly.
"""


#####################################################################


import sys, codecs, mimetypes
import random, string
from io import BytesIO
from .compat import (
    compat_cookiejar,
    compat_pickle
)


#####################################################################


class ClientCookieJar(compat_cookiejar.CookieJar):
    """Custom CookieJar that can be pickled to/from strings. """

    def __init__(self, cookie=None, policy=None):
        """Create ClientCookieJar class."""

        # Generate custom jar
        compat_cookiejar.CookieJar.__init__(self, policy)

        # Check cookie parameter
        if cookie:

            # Set cookie property
            self._cookies = compat_pickle.loads(cookie) \
                if isinstance(cookie, bytes) else \
                compat_pickle.loads(cookie.encode('utf-8'))

    @property
    def auth_expires(self):
        """Get expiration time for user identifier cookie. """

        # Iterate over cookies
        for cookie in self:

            # Check if cookie is the searched key
            if cookie.name in ('ds_user_id', 'ds_user'):

                # Return expiration
                return cookie.expires

        return None

    @property
    def expires_earliest(self):
        """For backward compatibility. """

        return self.auth_expires

    def dump(self):
        """Return normalized dictionary. """

        return compat_pickle.dumps(self._cookies)


class MultipartFormDataEncoder(object):
    """Custom MultipartFormData Encoder. """

    def __init__(self, boundary=None):
        """Create MultipartFormData class. """

        # Set boundary property
        self.boundary = boundary or \
            ''.join(random.choice(
                string.ascii_letters + string.digits + '_-'
            ) for _ in range(30))

        # Set content-type property
        self.content_type = 'multipart/form-data; boundary={}'.format(self.boundary)

    @classmethod
    def u(cls, s):
        """Decode string parameter easily. """

        # Check instance
        if sys.hexversion < 0x03000000 and isinstance(s, str) or \
           sys.hexversion >= 0x03000000 and isinstance(s, bytes):

            # Decode string
            s = s.decode('utf-8')

        return s

    def iter(self, fields, files):
        """Iterate over fields quickly. """

        # Get encoder
        __coder = codecs.getencoder('utf-8')

        # Iterate over fields
        for (key, value) in fields:
            __k = self.u(key)
            yield __coder('--{}\r\n'.format(self.boundary))
            yield __coder(self.u(
                'Content-Disposition: form-data; name="{}"\r\n'
            ).format(__k))
            yield __coder('\r\n')
            if isinstance(value, (int, float)):
                value = str(value)
            yield __coder(self.u(value))
            yield __coder('\r\n')

        # Iterate over files
        for (key, filename, content_type, fd) in files:
            __k = self.u(key)
            __file = self.u(filename)
            yield __coder('--{}\r\n'.format(self.boundary))
            yield __coder(self.u(
                'Content-Disposition: form-data; name="{}"; filename="{}"\r\n'
            ).format(__k, __file))
            yield __coder('Content-Type: {}\r\n'.format(
                content_type or
                mimetypes.guess_type(filename)[0] or
                'application/octet-stream')
            )
            yield __coder('Content-Transfer-Encoding: binary\r\n')
            yield __coder('\r\n')
            yield (fd, len(fd))
            yield __coder('\r\n')
        yield __coder('--{}--\r\n'.format(self.boundary))

    def encode(self, fields, files):
        """Encoder fields and fields quickly. """

        # Generate body
        __body = BytesIO()

        # Iterate over fields and files
        for chunk, _ in self.iter(fields, files):

            # Write to body
            __body.write(chunk)

        # Return normalized values
        return self.content_type, __body.getvalue()
