# Instagram Web API

A Python wrapper for the Instagram API with no 3rd party dependencies.

## Install

Install with pip:

``pip install git+https://gitlab.com/alejandrofcarrera/vg-instagram.git``

To update:

``pip install git+https://gitlab.com/alejandrofcarrera/vg-instagram.git --upgrade``

To update with latest repo code:

``pip install git+https://gitlab.com/alejandrofcarrera/vg-instagram.git --upgrade --force-reinstall``

Tested on Python 2.7 and 3.5.

## Legal

Disclaimer: This library is only focused to support ViralGroups functions.
